import { NowRequest, NowResponse } from '@now/node';
import axios from 'axios';
import 'full-icu';

const tempStr = '<article class="card"><header><h2 class="title">TITLE (DATE)</h2></header><pre class="content">CONTENT</pre></article>';

class Hw {
  title: string;
  content: string;
  date: Date;
  constructor(that: { title: string, content: string, date: Date | string | number }) {
    this.title = that.title;
    this.content = that.content;
    this.date = new Date(that.date);
  }
}

export default async function (_req: NowRequest, res: NowResponse) {
  const formatter = new Intl.DateTimeFormat('ru-RU', { day: 'numeric', month: 'long' })
  const { debug } = _req.query;
  if (debug) console.log("DEBUG");
  const origin = new URL(debug ? 'http://localhost:3000' : 'https://9v.mdim.now.sh');
  const pageUrl = `${origin.origin}/home.html`;
  let content = (await axios.get(pageUrl)).data as string;

  const hwUrl = 'https://bb.githack.com/tekirinka/9v/raw/master/src/hw.json';
  const hwsJSON = (await axios.get(hwUrl)).data.home as [{ title: string, content: string, date: string }];
  const hws = hwsJSON.map(x => new Hw(x));
  const hwSorted = hws.sort((a: Hw, b: Hw) => a.date.getTime() - b.date.getTime());

  const todayDate = new Date(Date.now());
  todayDate.setHours(0, 0, 0, 0);
  const today = todayDate.getTime();
  const tomorrow = today + 8.64e7;

  const parsedHw = hwSorted.map((hw: Hw) => {
    const date = hw.date.getTime();
    if (date < today) return;
    const dateStr =
      (date == today ?
        'Сегодня, ' :
        (date == tomorrow ?
          'Завтра, '
          : '')) + formatter.format(date);
    return tempStr
      .replace('TITLE', hw.title)
      .replace('CONTENT', hw.content)
      .replace('DATE', dateStr)
  }).join('');

  content = content.replace('HOMEWORK', parsedHw);

  return res.status(200).send(content);
}
