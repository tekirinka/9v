window.app = {
  init() {
    location.hash = location.hash || app.data.defaultHash;
    for (i in app.setup) app.setup[i]();
  },
  data: {
    defaultHash: "#home",
    template: document.querySelector("template.hw").content,
    hw: null,
    worker: null
  },
  setup: {
    unIframe() {
      window.parent.location = location;
    },
    scrollWorkaround() {
      setTimeout(_ => scrollTo(0, 0), 100);
      document
        .querySelectorAll("a")
        .forEach(item =>
          item.addEventListener("click", _ =>
            setTimeout(_ => window.scrollTo(0, 0), 100)
          )
        );
    },
    hw() {
      fetch("//raw.githack.com/tekirinka/mv-data/master/hw.json", {
        mode: "cors"
      })
        .then(x => x.text())
        .then(json => (app.data.hw = JSON.parse(json)))
        .then(_ => app.utils.showHW());
    },
    sw() {
      if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js");
      }
    },
    links() {
      const links = document.querySelectorAll("a[data-action]");
      for (link of links) {
        link.addEventListener(
          "click",
          app.utils.getNestedObjectByString(link.dataset.action)
        );
      }
    }
  },
  utils: {
    darken() {
      document.documentElement.style.background = "white";
      document.querySelectorAll("*").forEach(elt => (elt.style.color = "gray"));
      document
        .querySelectorAll("main > article")
        .forEach(elt => (elt.style.backgroundColor = "white"));
      document.documentElement.style.mixBlendMode = "exclusion";
      const meta = document.querySelector("meta[name=theme-color]");
      meta.content = "black";
    },
    lighten() {
      document.documentElement.style.background = "";
      document.querySelectorAll("*").forEach(elt => (elt.style.color = ""));
      document
        .querySelectorAll("main > article")
        .forEach(elt => (elt.style.backgroundColor = ""));
      document.documentElement.style.mixBlendMode = "";
      const meta = document.querySelector("meta[name=theme-color]");
      meta.content = "white";
    },
    showHW() {
      for (let hwContent in app.data.hw) {
        const val = app.data.hw[hwContent];
        val.sort((a, b) => new Date(a.date) - new Date(b.date));
        const oldNode = document.querySelector(`#${hwContent}`);
        let newNode = document.createElement(oldNode.tagName);
        newNode.classList = oldNode.classList;
        newNode.id = oldNode.id;
        for (let hwData of val) {
          let { title, content, date } = hwData;
          date = new Date(date);
          const dateNow = new Date(Date.now());
          dateNow.setHours(0, 0, 0, 0);
          if (date < dateNow) continue;
          let today =
              dateNow.getDate() == date.getDate()
                ? "сегодня, "
                : dateNow.getDate() + 1 == date.getDate()
                ? "завтра, "
                : "",
            dateText = date.toLocaleDateString();
          const hwNode = app.data.template.cloneNode(true);
          hwNode.querySelector(
            ".title"
          ).innerHTML = `${title} (${today}${dateText})`;
          hwNode.querySelector(".content").innerHTML = content;
          newNode.appendChild(hwNode);
        }
        oldNode.replaceWith(newNode);
      }
    },
    getNestedObjectByString(str, o = window) {
      let k = o;
      for (let i of str.split(".")) {
        k = k[i] || undefined;
      }
      return k;
    }
  }
};

window.addEventListener("load", app.init);
