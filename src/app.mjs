window.app = {
init() {
    location.hash = location.hash || app.data.defaultHash;
    for (const i in app.setup) app.setup[i]();
  },
  data: {
    defaultHash: "#home"
  },
  setup: {
    unIframe() {
      window.parent.location = location;
    },
    scrollWorkaround() {
      setTimeout(_ => scrollTo(0, 0), 100);
      document
        .querySelectorAll("a")
        .forEach(item =>
          item.addEventListener("click", _ =>
            setTimeout(_ => window.scrollTo(0, 0), 100)
          )
        );
    },
    sw() {
      if ("serviceWorker" in navigator) {
        navigator.serviceWorker.register("/sw.js");
      }
    },
    links() {
      const links = document.querySelectorAll("a[data-action]");
      for (const link of links) {
        link.addEventListener(
          "click",
          app.utils.getNestedObjectByString(link.dataset.action)
        );
      }
    }
  },
  utils: {
    darken() {
      document.documentElement.style.background = "white";
      document.querySelectorAll("*").forEach(elt => (elt.style.color = "gray"));
      document
        .querySelectorAll("main > article")
        .forEach(elt => (elt.style.backgroundColor = "white"));
      document.documentElement.style.mixBlendMode = "exclusion";
      const meta = document.querySelector("meta[name=theme-color]");
      meta.content = "black";
    },
    lighten() {
      document.documentElement.style.background = "";
      document.querySelectorAll("*").forEach(elt => (elt.style.color = ""));
      document
        .querySelectorAll("main > article")
        .forEach(elt => (elt.style.backgroundColor = ""));
      document.documentElement.style.mixBlendMode = "";
      const meta = document.querySelector("meta[name=theme-color]");
      meta.content = "white";
    },
    getNestedObjectByString(str, o = window) {
      let k = o;
      for (let i of str.split(".")) {
        k = k[i] || undefined;
      }
      return k;
    }
  }
};

window.addEventListener("load", app.init);
